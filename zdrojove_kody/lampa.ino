#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>
#include <cstdlib>
#include "DHT.h"

//***ASYNC
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
AsyncWebServer server(80);
//***
int zelena,cervena,modra; 
//----------------------------------------------------------------------------------------
#define DHTPIN D3    // Digital pin connected to the DHT sensor
#define DHTTYPE DHT11 // DHT 11
#define LED_PIN     D1  // Pin connected to the LED strip
#define NUM_LEDS    60  // Number of LEDs in the strip

Adafruit_NeoPixel rgbWS = Adafruit_NeoPixel(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);
DHT dht(DHTPIN, DHTTYPE);
int currentMode = -1;

const char* ssid = "3301-Iot";
const char* password = "mikrobus";
const char* mqtt_server = "broker.hivemq.com";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

void setup_wifi() {

  delay(10);
  
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  //***
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", "Ověření, jestli elegantOTA funguje");
  });

  AsyncElegantOTA.begin(&server);    // Start AsyncElegantOTA
  server.begin();
  Serial.println("HTTP server started");
  //***
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if(strcmp(topic,"klicka/moodlamp/pasek/rgb")==0){
    currentMode = -1;
    setStripRGB(payload, length);
  }

  if(strcmp(topic,"klicka/moodlamp/pasek/efekty")==0){    
    if((char)payload[0] == '0'){    //OFF
      currentMode = -1;
      setStripMode0();
    }
    if((char)payload[0] == '1'){    //RED
      currentMode = -1;
      setStripMode1();
    }
    if((char)payload[0] == '2'){    //BLUE
      //setStripMode2();
      currentMode = 2;
    }
    if((char)payload[0] == '3'){    //LAVA
      currentMode = -1;
      setStripMode3();
    }
  }
}

void reconnect() {
  
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.publish("klicka/moodlamp/test", "Zapnuto");
      client.subscribe("klicka/moodlamp/pasek/efekty");
      client.subscribe("klicka/moodlamp/pasek/rgb");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

//------------------------------------------------------------------------

void setup() {
  //---------------
  rgbWS.begin();
  //---------------
  dht.begin();
  //---------------
  
  pinMode(BUILTIN_LED, OUTPUT);
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  //---
  startMoodLamp();
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  //---
  unsigned long now2 = millis();
  if(now2 - lastMsg > 2000){
    lastMsg = now2;
    readTemperatureAndHumidity();
  }
  //---

  unsigned long now = millis();
  if (now - lastMsg > 10000) {
    lastMsg = now;
    ++value;
    snprintf (msg, MSG_BUFFER_SIZE, "Automatická kontrolní zpráva #%ld", value);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("klicka/moodlamp/test", msg);
  }

  if (currentMode == 2){
    setStripMode2(1);
  }
  
}

//------------------------------------------------------------------------

void nastavRGB (byte r, byte g, byte b, int cislo) {
  uint32_t barva;
  barva = rgbWS.Color(r, g, b);
  rgbWS.setPixelColor(cislo - 1, barva);
  rgbWS.show();
}

void startMoodLamp(){
  for(int j=0; j<3; j++){
    for(int i=1; i<=60; i++){
      nastavRGB(255,0,0,i);
    }
    delay(10);
    for(int i=1; i<=60; i++){
      nastavRGB(0,0,0,i);
    }
    delay(10);
  }

  
}

void setStripRGB(byte* payload, unsigned int length){                       //RGB 
  payload[length] = '\0'; // Přidání ukončovací nuly pro vytvoření řetězce
    String payloadStr = String((char*)payload);

    // Hledání hodnot r, g, b ve formátu "rgb(x, y, z)"
    int r, g, b;
    if (sscanf(payloadStr.c_str(), "rgb(%d, %d, %d)", &r, &g, &b) == 3) {
      Serial.print("r = ");
      Serial.print(r);
      Serial.print(" | g = ");
      Serial.print(g);
      Serial.print(" | b = ");
      Serial.println(b);

      for(int i=1; i<=60; i++){
        nastavRGB(r,g,b,i);
      }
    } else {
      Serial.println("Invalid RGB format");
    }
}
void setStripMode0(){               //OFF
  for(int i=1; i<=60; i++){
      nastavRGB(0,0,0,i);
  }
}
void setStripMode1(){               //Red
  
  for(int i=0; i<=60; i++){
      nastavRGB(255,0,0,i);
  }
}

void setStripMode2(int speed){               //BLUE
  for(int i=0; i<=60; i++){
      nastavRGB(0,0,255,i);
  }
}

void setStripMode3(){               //LAVA
  for(int i=1; i<=19; i++){
      nastavRGB(255,0,0,i);
  }
  for(int i=20; i<=60; i++){
      nastavRGB(255,165,0,i);
  }
}


//---

void readTemperatureAndHumidity(){
  float tep = dht.readTemperature();
  float vlh = dht.readHumidity();
  if (isnan(tep) || isnan(vlh)) {
    Serial.println("Chyba při čtení z DHT senzoru!");
  } else {
    Serial.print("Teplota: "); 
    Serial.print(tep);
    Serial.print("  °C, ");
    Serial.print("Vlhkost: "); 
    Serial.print(vlh);
    Serial.println("  %");

    String tepStr = String(tep, 2); // Převod float na řetězec s dvěma desetinnými místy
    String vlhStr = String(vlh, 2); // Převod float na řetězec s dvěma desetinnými místy
    client.publish("klicka/moodlamp/teplota", tepStr.c_str());
    client.publish("klicka/moodlamp/vlhkost", vlhStr.c_str());

   
  }
}
