# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | 5 hodin                                 |
| odkud jsem čerpal inspiraci                   | Google obrázky                          |
| odkaz na video                                | https://youtu.be/Mp43YmHTxsU                    |
| jak se mi to podařilo rozplánovat             | docela dobře                                 |
| proč jsem zvolil tento design                 | co bylo k dispozici                                |
| zapojení                                      | https://gitlab.spseplzen.cz/klickav/4h-projekt-mood-lamp/-/blob/main/dokumentace/schema/foto_schema_zapojeni.jpg      |
| z jakých součástí se zapojení skládá          | ESP8266,WS2812 Led pasek, DHT22 čidlo teploty a vlhkosti                                |
| realizace                                     | https://gitlab.spseplzen.cz/klickav/4h-projekt-mood-lamp/-/blob/main/dokumentace/fotky/434799198_404502688962636_6990930349808734652_n.jpg |
| UI                                            | https://gitlab.spseplzen.cz/klickav/4h-projekt-mood-lamp/-/blob/main/dokumentace/fotky/434864809_440551005100906_6976061886155599280_n.jpg               |
| co se mi povedlo                              | zapojení                                |
| co se mi nepovedlo/příště bych udělal/a jinak | design                                |
| zhodnocení celé tvorby (návrh známky)         | s ohledem na okolnosti 3                                |
